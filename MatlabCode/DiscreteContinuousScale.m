
% To show that even when using the sampled kernel, it would not be that bad
% if sigma is large enough.

set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');
set(0,'defaultTextInterpreter','latex'); 
s = 2;
sigma = sqrt(s);
x = floor(-3*sigma):ceil(3*sigma);
Discrete = besseli(x,s,1);
Continuous = normpdf(x,0,sqrt(s));
figure;
stem(x,Discrete,'r-o')
hold on
stem(x,Continuous,'k-o')
plot(x,Discrete,'r')
plot(x,Continuous,'k')
legend('T[n,s = 3] ','G[n,s = 3]');
%xlim([-3*s - 0.5 3*s + 0.5])
xlabel('n')
ylabel(' G[n,s],T[n,s]')

s = 2;
%x = -3*s:3*s;
Discrete = besseli(x,s^2,1); %* exp(-s);
Continuous = normpdf(x,0,s);
%figure;
stem(x,Discrete,'r-d')
hold on
stem(x,Continuous,'k-d')
plot(x,Discrete,'r')
plot(x,Continuous,'k')
legend('G[n,s = 2] ','T[n,s = 2]');
%xlim([-3*s - 0.5 3*s + 0.5])
xlabel('n')
ylabel(' G[n,s],T[n,s]');
%figure;
s = 1;
%x = -3*s:3*s;
Discrete = besseli(x,s^2,1); %* exp(-s);
Continuous = normpdf(x,0,s);
stem(x,Discrete,'r-*')
hold on
stem(x,Continuous,'k-*')
plot(x,Discrete,'r')
plot(x,Continuous,'k')
legend('G[n,s = 1] ','T[n,s = 1]');
%xlim([-3*s - 0.5 3*s + 0.5])
xlabel('n')
ylabel('G[n,s],T[n,s]')

legend('G[n,s = 3] ','T[n,s = 3]','G[n,s = 2] ','T[n,s = 2]','G[n,s = 1] ','T[n,s = 1]') 
%figure;
s = 4;
x = -3*s:3*s;
Discrete = besseli(x,s^2,1); %* exp(-s);
Continuous = normpdf(x,0,s);
stem(x,Discrete,'r-o')
hold on
stem(x,Continuous,'k-o')
legend('G[n,s = 4] ','T[n,s = 4]');
xlim([-3*s - 0.5 3*s + 0.5])
xlabel('n')
ylabel('G[n,s],T[n,s]')


% The actually important plot:

val = zeros(1,50);
for s = 1:50
    x = -4*s:4*s;
    sigma = sqrt(s);
    Discrete = besseli(x,s,1); %* exp(-s);
    Continuous = normpdf(x,0,sqrt(s));
    val(s) = (sum(abs(Discrete-Continuous)));
end
figure; semilogy(val);
xlabel('s')
ylabel('$|G-T|$')
figure; semilogy(val);
xlabel('s')
ylabel('$|G-T|$')

