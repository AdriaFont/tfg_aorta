% Plot any number of diffisivity profiles choosing the minimum, maximum and
% interval value of k

set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');
set(0,'defaultTextInterpreter','latex');
x = 0:0.01:1;
mink = 0.1;
maxk = 2.1;
stepk = 0.3;
L =length(mink:stepk:maxk);
Legend=cell(L,1);
figure;hold on;
counter = 0;
for k = mink:stepk:maxk
     counter = counter +1;
     g_x = 1./(1+(x./k).^2);
     plot(x,g_x,'color',[k/maxk 0 1-k/maxk]);
     Legend{counter} =  sprintf('k =%01f',k); 
end
legend(Legend);
ylabel('$ g(x) $')
xlabel('$ x $')
 

