Bachelor Thesis. Multiscale Tubular Features and Representation. Adri� Font Calvarons. 2016.

MATLAB Scripts:

********** DiscreteContinuousScale.m **********
Implements Fig. 2.2 in the manuscript. Output Figure is tunable. 
Provides additional illustrative figures.

***********************************************

********** DiffusivityCoeff.m *****************
Implements Fig. 2.3 in the manuscript. Output Figure is tunable.

***********************************************

********** GaussianHermiteOrder.m *****************
Implements Fig. 2.9 in the manuscript. Output Figure is tunable.

***********************************************