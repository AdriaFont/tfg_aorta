
% Computes and plots N number of 1D gaussian (1,...,N)-th derivatives.
set(0,'defaultTextInterpreter','latex');
s = 5;
x = -4*s:4*s;
N = 10;
yplot = ceil(N/2);
xplot = floor((N-yplot)/2);
figure;subplot(xplot,yplot,N)
for l = 1:N   
dG_l = (-1)^l*(1/(sqrt(2)*s))^l * hermiteH(l,x/(s*sqrt(2))) ... % Hermite polynomial
    .*  1/(s*sqrt(2*pi)).* exp(-(x.*x)/(2*s^2)); % 1D Gaussian kernel
subplot(xplot,yplot,l);plot(dG_l); axis off
str = sprintf('l = %i',l);
title(str);
end


