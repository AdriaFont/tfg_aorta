Bachelor Thesis. Multiscale Tubular Features and Representation. Adrià Font Calvarons. 2016.

C++ Scripts, required: OpenCV and ITK (including ItkVideoBridgeOpenCV,ITKReview):
First version, Updates to come soon.

********** VesselSegmentationMiao.cxx **********
Preprocessing + Frangi Filter.

Based on the methods (section 3.1) in Miao, Shun, Rui Liao, and Marcus
Pfister. "Toward smart utilization of two X-ray images for 2-D/3-D
registration applied to abdominal aortic aneurysm interventions."
Computers & Electrical Engineering 39.5 (2013): 1485-1498. Section 3.
2D x-ray image processing.

IMPORTANT: The original itk class:
			itk::MultiScaleHessianBasedMeasureImageFilter
		   has been modified, and is provided along with the source file.

VesselSegmentationMiao expects as image input:
  Either:
  A series of DSA .tif images as input (e.g image001.tif, image002.tif,...Image%03d.tif).
  A single 2D image of any format supported in ITK.
  A single 3D image stack.

VesselSegmentationMiao expects as parameter input:
  A series of parameters, type VesselSegmentationMiao (no arguments) in the command line to get a list of needed parameters.
  
VesselSegmentationMiao produces the following outputs:
  A preprocessed image ready to undergo the tubularity measures (float).
  The Multiscale output of the Frangi Filter (float).
  The scale responses of the Frangi Output (float).
  The most successful response of the Scale responses (float).
 
************************************************
  
********** OOFFilter.cxx ***********************
OOF Filter.

DISCLAIMER: All credit for the implementation of the classes used, goes to the authors in:
Benmansour, Fethallah, Engin Türetken, and Pascal Fua.
Tubular geodesics using oriented flux: an ITK implementation. No. EPFL-ARTICLE-183637. 2013.
IMPORTANT: The original itk classes have been modified in order to be made compatible for ITK 4.7 and higher:
the class ::itk::GetImageDimension< TInputImage >::ImageDimension is replaced with TInputImage::ImageDimension.

OOFFilter expects as image input:
  A single image, tipically the preprocessd image output from VesselSegmentationMiao.exe (float)

OOFFilter expects as parameter input:
  A series of parameters, type OOFFilter (no arguments) in the command line to get a list of needed parameters.
  
OOFFilter produces the following outputs:
  The Multiscale output of the OOF Filter (float).
  The scale responses of the OOF Output (float).
  The most successful response of the Scale responses (float).
  
************************************************

********** SteerableFilter.cxx *****************
2-nd and 4-th order Steerable Filters.

SteerableFilter expects as image input:
  A single image, tipically the preprocessd image output from VesselSegmentationMiao.exe (float).

SteerableFilter expects as parameter input:
  A series of parameters, type SteerableFilter (no arguments) in the command line to get a list of needed parameters.
  
SteerableFilter produces the following outputs:
  The Multiscale output of the 2-nd order steerable filter (float).
  The Multiscale output of the 4-th order steerable filter (float).
  The scale responses of the 2-nd order steerable output (float).
  The scale responses of the 4-th order steerable output (float).
  
************************************************

********** FeatureTile.cxx *****************
Feature Selection. Combines 2 feature stacks for a given image

FeatureTile expects as image inputs:
  A 1st stack of filter responses (float).
  A 2nd stack of filter responses (float).
 (Same image size, naturally same original image)

  
FeatureTile produces the following output:
  A stack of combined features (float).
  
Typical use:
Combine in a script according to the features to be tiled. Example (windows):

 FeatureTile.exe FeatureStack1.mhd TwoTile/FeatureStack2.mhd  TwoTile/interm.mhd
 FeatureTile.exe TwoTile/interm.mhd TwoTile/FeatureStack3.mhd TwoTile/interm.mhd
 FeatureTile.exe TwoTile/interm.mhd TwoTile/FeatureStack4.mhd TwoTile/interm.mhd
 FeatureTile.exe TwoTile/interm.mhd TwoTile/FeatureStack5.mhd TwoTile/interm.mhd
 FeatureTile.exe TwoTile/interm.mhd TwoTile/FeatureStack6.mhd TwoTile/interm.mhd
 FeatureTile.exe TwoTile/interm.mhd TwoTile/FeatureStack7.mhd TwoTile/interm.mhd
 FeatureTile.exe TwoTile/interm.mhd TwoTile/FeatureStack7.mhd TwoTile/FeatureSelected.mhd
  
************************************************
  
********** MLex.cxx *****************
Feature Selection. Combines 2 feature stacks for a given image

MLex expects as image inputs either:
  Arbitrary amount N of training stacks (float) and
  Arbitrary amount N of corresponding label sets (float).
  or (in the next update)
  SVM file (.xls) and additional files to load.

MLex expects as parameter input:
  A series of parameters, type MLex (no arguments) in the command line to get a list of needed parameters.
  
MLex produces the following output:
  A segmentation output.
  if SVM is to be saved (in the next update):
	a SVM file (.xls) and additional files.
	
************************************************



