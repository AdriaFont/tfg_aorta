
// Adri� Font Calvarons

/*=========================================================================
*
*  Copyright Insight Software Consortium
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0.txt
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*=========================================================================*/

#include "itkImage.h"
#include "math.h"
#include "itkImportImageFilter.h"
#include "itkImageFileWriter.h"
#include "itkCastImageFilter.h"
#include "itkFFTConvolutionImageFilter.h"
#include <itkNaryAddImageFilter.h>
#include "itkImageFileReader.h"
#include "itkMultiplyImageFilter.h"
#include <itkShapedNeighborhoodIterator.h>
#include "itkTileImageFilter.h"
#include <itkImageSliceConstIteratorWithIndex.h>
#include <itkJoinSeriesImageFilter.h>
#include "vnl/vnl_math.h"
#include "itkImageLinearIteratorWithIndex.h"
#include "itkExtractImageFilter.h"
#include <itkImageSliceIteratorWithIndex.h>
#define PI 3.14159265358979323846

#include "QuickView.h"

typedef double   PixelType;
const unsigned int Dimension = 2;

typedef itk::Image< PixelType, Dimension > ImageType;

typedef itk::Image< PixelType, 3 > ImageType3D;

typedef itk::ImageLinearIteratorWithIndex< ImageType > LinearIteratorType;

typedef itk::ImageSliceConstIteratorWithIndex< ImageType3D > SliceConstIteratorType;

typedef itk::ImageSliceIteratorWithIndex< ImageType3D > SliceIteratorType;


static void MaxProjection(ImageType3D::Pointer inputImage, ImageType::Pointer outputImage);
static void Sumer(ImageType3D::Pointer inputImage, ImageType3D::Pointer outputImage);

int main(int argc, char * argv[])
{
	if (argc < 8)
	{
		std::cerr << "Usage: " << std::endl;
		std::cerr << argv[0] << " inputImageFile 2ndorderScaleImageFile 4thorderScaleImageFile 2ndorderoutImageFile 4thorderoutImageFile sigmamin sigmamax sigmastep " << std::endl;
		return EXIT_FAILURE;
	}
	
	

	typedef itk::Image< double, Dimension > ImageTypeRead;

	typedef itk::ImportImageFilter< PixelType, 3 >   ImportFilterType;

	typedef itk::ImageFileReader<ImageTypeRead>        ReaderType;

	typedef itk::FFTConvolutionImageFilter<ImageType> FilterType;

	typedef itk::MultiplyImageFilter<ImageType, ImageType, ImageType> MultiplyImageFilterType;

	typedef  itk::NaryAddImageFilter<ImageType3D, ImageType3D> AddFilterType;

	typedef itk::CastImageFilter< ImageTypeRead, ImageType > CastFilterType;
  
	typedef itk::ExtractImageFilter< ImageType3D, ImageType > ExtractFilterType;

	typedef itk::JoinSeriesImageFilter< ImageType, ImageType3D> JoinSeriesFilterType;

	typedef itk::TileImageFilter< ImageType, ImageType3D > TileFilterType;

	typedef itk::ImageFileWriter< ImageType > WriterType;

	typedef itk::ImageFileWriter< ImageType3D > WriterType3D;

	
	ReaderType::Pointer reader = ReaderType::New();
	reader->SetFileName(argv[1]);

	CastFilterType::Pointer filterCast = CastFilterType::New();
	filterCast->SetInput(reader->GetOutput());

	ImageType::Pointer Imread = reader->GetOutput();

	JoinSeriesFilterType::Pointer joinSeriesSigma2 = JoinSeriesFilterType::New();
	joinSeriesSigma2->SetOrigin(0);
	joinSeriesSigma2->SetSpacing(1);
	JoinSeriesFilterType::Pointer joinSeriesSigma4 = JoinSeriesFilterType::New();
	joinSeriesSigma4->SetOrigin(0);
	joinSeriesSigma4->SetSpacing(1);

	const double sigmamin = atoi(argv[6]);
	const double sigmamax = atoi(argv[7]);
	const double sigmastep = atoi(argv[8]);
	const int sigmainterval = int((sigmamax - sigmamin) /(sigmastep-1));
	std::cout << " sigmaintrval: " << sigmainterval;
	for (double sigma = sigmamin; sigma <= sigmamax; sigma = sigma + sigmainterval){
		std::cout << " sigma: " << sigma;
		double sigmasize = sigma * 3;

		ImportFilterType::SizeType  size;
		size[1] = sigmasize * 2;  // size along Y
		size[0] = sigmasize * 2;  // size along X
		size[2] = 8;
		ImportFilterType::IndexType start;
		start.Fill(0);
		ImportFilterType::RegionType region;
		region.SetIndex(start);
		region.SetSize(size);


		const itk::SpacePrecisionType origin[Dimension] = { 0.0, 0.0 };


		// spacing isotropic volumes to 1.0
		const itk::SpacePrecisionType  spacing[Dimension] = { 1.0, 1.0 };


		//Allocate the memory block containing the pixel data to be
		// passed to import
		const unsigned int imSize = size[0] * size[1];
		const unsigned int numberOfPixels = imSize * 8;
		PixelType * localBuffer0 = new PixelType[numberOfPixels];

		PixelType * itxy = localBuffer0;
		PixelType * itxx = localBuffer0 + imSize * 1;
		PixelType * ityy = localBuffer0 + imSize * 2;
		PixelType * itxxyy = localBuffer0 + imSize * 3;
		PixelType * itxxxy = localBuffer0 + imSize * 4;
		PixelType * itxyyy = localBuffer0 + imSize * 5;
		PixelType * itxxxx = localBuffer0 + imSize * 6;
		PixelType * ityyyy = localBuffer0 + imSize * 7;
		// PixelType* it_i = localBuffer0 + i*N*N

		double g0;
		double gxx;
		double gyy;
		double gxy;
		double gxxyy;
		double gxxxx;
		double gyyyy;
		double gxxxy;
		double gxyyy;


		double sum = 0;
		double sum2 = 0;
		for (unsigned int x = 0; x < size[0]; x++)
		{
			const double xind = static_cast<double>(x)
				-static_cast<double>(size[0]) / 2.0;
			for (unsigned int y = 0; y < size[1]; y++)
			{
				const double yind = static_cast<double>(y)
					-static_cast<double>(size[1]) / 2.0;
				// gaussian Kernel. Watch out confusion b/ sigma an scale s scale space theory 
				g0 = 1 / (2 * PI * pow(sigma, 2.0)) * exp(-(pow(yind, 2.0) + pow(xind, 2.0)) / (2.0 *pow(sigma, 2.0)));
				// derivatives derived in notebook:
				gxy = xind * yind / pow(sigma, 4.0) * g0;
				gxx = (pow(xind, 2.0) - pow(sigma, 2.0)) / pow(sigma, 4.0) * g0;
				gyy = (pow(yind, 2.0) - pow(sigma, 2.0)) / pow(sigma, 4.0) * g0;
				gxxyy = -((pow(xind, 2.0) - pow(sigma, 2.0)) *(pow(yind, 2.0) - pow(sigma, 2.0))) / pow(sigma, 8.0) * g0;
				gxxxx = -(pow(xind, 4.0) - 6 * pow(xind, 2.0) * pow(sigma, 2.0) + 3 * pow(sigma, 4.0)) / pow(sigma, 8.0) * g0;
				gyyyy = -(pow(yind, 4.0) - 6 * pow(yind, 2.0) * pow(sigma, 2.0) + 3 * pow(sigma, 4.0)) / pow(sigma, 8.0) * g0;
				gxxxy = -((pow(xind, 3.0) - 3 * xind * pow(sigma, 2.0))* yind) / pow(sigma, 8.0) * g0;
				gxyyy = -((pow(yind, 3.0) - 3 * yind * pow(sigma, 2.0))* xind) / pow(sigma, 8.0) * g0;

				*itxy++ = gxy;
				*itxx++ = gxx;
				*ityy++ = gyy;
				*itxxyy++ = gxxyy;
				*itxxxy++ = gxxxy;
				*itxyyy++ = gxyyy;
				*itxxxx++ = gxxxx;
				*ityyyy++ = gyyyy;

				sum += gxxyy;

			}
		}


		ImportFilterType::Pointer importFilter = ImportFilterType::New();


		const bool importImageFilterWillOwnTheBuffer = true;
		importFilter->SetImportPointer(localBuffer0, numberOfPixels,
			importImageFilterWillOwnTheBuffer);
		importFilter->SetRegion(region);
		importFilter->SetOrigin(origin);
		importFilter->SetSpacing(spacing);
		importFilter->Update();
		ImageType3D::Pointer kernels = importFilter->GetOutput();


		std::cout << " kernels created";

		/*typedef itk::Image< float, 3 > ImageTypeZ;
		typedef itk::ImageFileWriter< ImageTypeZ > WriterTypeZ;
		typedef itk::CastImageFilter< ImageType3D, ImageTypeZ > CastFilterType;
		CastFilterType::Pointer castFilter = CastFilterType::New();
		castFilter->SetInput(kernels);

		WriterTypeZ::Pointer writerSCL = WriterTypeZ::New();
		writerSCL->SetFileName("Kernels.tif");
		writerSCL->SetInput(castFilter->GetOutput());
		try
		{
			writerSCL->Update();
		}
		catch (itk::ExceptionObject & exp)
		{
			std::cerr << "Exception caught !" << std::endl;
			std::cerr << exp << std::endl;
			return EXIT_FAILURE;
		}*/
		
		


		ImageType3D::RegionType requestedRegion = kernels->GetLargestPossibleRegion();

		SliceConstIteratorType inIterator(kernels, kernels->GetLargestPossibleRegion());
		inIterator.SetFirstDirection(0); ///x axis
		inIterator.SetSecondDirection(1); ///y axis

		/////Setup Image Stack that will be Joined together
		JoinSeriesFilterType::Pointer joinSeries = JoinSeriesFilterType::New();
		joinSeries->SetOrigin(kernels->GetOrigin()[2]);
		joinSeries->SetSpacing(kernels->GetSpacing()[2]);
		
		for (inIterator.GoToBegin(); !inIterator.IsAtEnd(); inIterator.NextSlice())
		{

			//Setup region of the slice to extract
			ImageType3D::IndexType sliceIndex = inIterator.GetIndex();
			ExtractFilterType::InputImageRegionType::SizeType sliceSize = inIterator.GetRegion().GetSize();
			sliceSize[2] = 0;
			ExtractFilterType::InputImageRegionType sliceRegion = inIterator.GetRegion();
			sliceRegion.SetSize(sliceSize);
			sliceRegion.SetIndex(sliceIndex);
			///Pull out slice
			
			ExtractFilterType::Pointer inExtractor = ExtractFilterType::New(); //Must be within loop so that smart pointer is unique
			inExtractor->SetInput(kernels);
			inExtractor->SetExtractionRegion(sliceRegion);
			inExtractor->SetDirectionCollapseToIdentity();
			inExtractor->Update();

		
			///Operate on Slice - Convolve
			FilterType::Pointer convolutionFilter = FilterType::New();
			convolutionFilter->SetInput(Imread);
			convolutionFilter->SetKernelImage(inExtractor->GetOutput());
			// View Kernel
			//QuickView viewer;
			//viewer.AddImage(inExtractor->GetOutput());
			//viewer.Visualize();
			try
			{
				convolutionFilter->Update();
			}
			catch (itk::ExceptionObject & exp)
			{
				std::cerr << "Exception caught !" << std::endl;
				std::cerr << exp << std::endl;
				return EXIT_FAILURE;
			}
			joinSeries->PushBackInput(convolutionFilter->GetOutput());

		}
		joinSeries->Update();
		ImageType3D::Pointer Responses = joinSeries->GetOutput();
		//ImageType3D::Pointer Responses = tileFilter->GetOutput();
		std::cout << " Convolution done";
		SliceConstIteratorType outIterator(Responses, Responses->GetLargestPossibleRegion());
		outIterator.SetFirstDirection(0); ///x axis
		outIterator.SetSecondDirection(1); ///y axis


		JoinSeriesFilterType::Pointer joinSeries4angle = JoinSeriesFilterType::New();
		joinSeries4angle->SetOrigin(Responses->GetOrigin()[2]);
		joinSeries4angle->SetSpacing(Responses->GetSpacing()[2]);

		//Initialize sumer

		ImageType3D::RegionType regionSumer;
		ImageType3D::RegionType::SizeType sizeSumer;
		ImageType3D::RegionType::IndexType indexSumer;
		ImageType3D::RegionType requestedRegionSumer = Responses->GetLargestPossibleRegion();

		indexSumer[0] = requestedRegionSumer.GetIndex()[0];
		indexSumer[1] = requestedRegionSumer.GetIndex()[1];
		indexSumer[2] = 0;
		sizeSumer[0] = requestedRegionSumer.GetSize()[0];
		sizeSumer[1] = requestedRegionSumer.GetSize()[1];
		sizeSumer[2] = 13;
		regionSumer.SetSize(sizeSumer);
		regionSumer.SetIndex(indexSumer);

		ImageType3D::Pointer outputImageSumer2 = ImageType3D::New();
		outputImageSumer2->SetRegions(regionSumer);
		outputImageSumer2->Allocate();
		outputImageSumer2->FillBuffer(0);

		ImageType3D::Pointer outputImageSumer4 = ImageType3D::New();
		outputImageSumer4->SetRegions(regionSumer);
		outputImageSumer4->Allocate();
		outputImageSumer4->FillBuffer(0);



		//Initialize adder;

		AddFilterType::Pointer addFilter2 = AddFilterType::New();
		AddFilterType::Pointer addFilter4 = AddFilterType::New();
		// create 0 image to add first
		ImageType::Pointer Emptyimage = ImageType::New();
		int counter = 0;
		for (outIterator.GoToBegin(); !outIterator.IsAtEnd(); outIterator.NextSlice())
		{

			///Setup region of the slice to extract
			ImageType3D::IndexType sliceIndex = outIterator.GetIndex();
			ExtractFilterType::InputImageRegionType::SizeType sliceSize = outIterator.GetRegion().GetSize();
			sliceSize[2] = 0;
			ExtractFilterType::InputImageRegionType sliceRegion = inIterator.GetRegion();
			sliceRegion.SetSize(sliceSize);
			sliceRegion.SetIndex(sliceIndex);

			///Pull out slice
			ExtractFilterType::Pointer outExtractor = ExtractFilterType::New(); ///Must be within loop so that smart pointer is unique
			outExtractor->SetInput(Responses);
			outExtractor->SetExtractionRegion(sliceRegion);
			outExtractor->SetDirectionCollapseToIdentity();
			outExtractor->Update();

			JoinSeriesFilterType::Pointer joinSeriesAngle = JoinSeriesFilterType::New();
			joinSeriesAngle->SetOrigin(Responses->GetOrigin()[2]);
			joinSeriesAngle->SetSpacing(Responses->GetSpacing()[2]);

			///Operate on Slice
			double Interp[8];
			int gamma = 1; // gamma normalization --> [(sigma)^2]^(n * gamma / 2)
			for (int indtheta = 0; indtheta <= 180; indtheta = indtheta + 15){

				float theta = indtheta * PI / 180.0;
				// Derivative interpolation functions f(theta) and gamma-normalized derivatives f(sigma,gamman,n-deriv) :
				Interp[1] = pow(cos(theta), 2) * pow(sigma, 2*2 * gamma/2);
				Interp[0] = 2 * cos(theta) * sin(theta) * pow(sigma, 2* 2 * gamma / 2);
				Interp[2] = pow(sin(theta), 2) * pow(sigma, 2 * 2 * gamma / 2);
				Interp[4] = 4 * pow(cos(theta), 3) * sin(theta) * pow(sigma, 2 * 4 * gamma / 2);
				Interp[5] = 4 * pow(sin(theta), 3) * cos(theta) * pow(sigma, 2 * 4 * gamma / 2);
				Interp[3] = 6 * pow(cos(theta), 2) * pow(sin(theta), 2) * pow(sigma, 2 * 4 * gamma / 2);
				Interp[6] = pow(cos(theta), 4) * pow(sigma, 2 * 4 * gamma / 2);
				Interp[7] = pow(sin(theta), 4) * pow(sigma, 2 * 4 * gamma / 2);

				MultiplyImageFilterType::Pointer outmultiplyImageFilter = MultiplyImageFilterType::New();
				outmultiplyImageFilter->SetInput(outExtractor->GetOutput());
				outmultiplyImageFilter->SetConstant(Interp[counter]);
				outmultiplyImageFilter->Update();
				ImageType::Pointer resp = outmultiplyImageFilter->GetOutput();
				resp->DisconnectPipeline();

				joinSeriesAngle->PushBackInput(resp);

			}
			try
			{
				joinSeriesAngle->Update();
			}
			catch (itk::ExceptionObject & exp)
			{
				std::cerr << "Exception caught !" << std::endl;
				std::cerr << exp << std::endl;
				return EXIT_FAILURE;
			}
			ImageType3D::Pointer SlicesAngles = joinSeriesAngle->GetOutput();
			ImageType3D::RegionType aa = SlicesAngles->GetLargestPossibleRegion();

			///Save Slice

			if (counter < 3){
				Sumer(SlicesAngles, outputImageSumer2);
			//	addFilter2->SetInput(counter, SlicesAngles);
			}
			else{
				Sumer(SlicesAngles, outputImageSumer4);
			//	addFilter4->SetInput(counter - 3, SlicesAngles);
			}

			// Add  3D slice manually
			
			//Update counter
			counter = counter + 1;
		}
		
		std::cout << " adding done ";
		ImageType::RegionType regionangle;
		ImageType::RegionType::SizeType sizeangle;
		ImageType::RegionType::IndexType indexangle;
		ImageType3D::RegionType requestedRegionAngle = outputImageSumer2->GetLargestPossibleRegion();

		indexangle[0] = requestedRegionAngle.GetIndex()[0];
		indexangle[1] = requestedRegionAngle.GetIndex()[1];
		sizeangle[0] = requestedRegionAngle.GetSize()[0];
		sizeangle[1] = requestedRegionAngle.GetSize()[1];
		regionangle.SetSize(sizeangle);
		regionangle.SetIndex(indexangle);

		ImageType::Pointer outputImage2 = ImageType::New();
		outputImage2->SetRegions(regionangle);
		outputImage2->Allocate();
		MaxProjection(outputImageSumer2, outputImage2);

		ImageType::Pointer outputImage4 = ImageType::New();
		outputImage4->SetRegions(regionangle);
		outputImage4->Allocate();
		MaxProjection(outputImageSumer4, outputImage4);

		// join ImageFilter to join scales
		joinSeriesSigma2->PushBackInput(outputImage2);
		joinSeriesSigma4->PushBackInput(outputImage4);

	}

	try
	{
		joinSeriesSigma2->Update();
		joinSeriesSigma4->Update();
	}
	catch (itk::ExceptionObject & exp)
	{
		std::cerr << "Exception caught !" << std::endl;
		std::cerr << exp << std::endl;
		return EXIT_FAILURE;
	}
	ImageType3D::Pointer ScaleResponses2 = joinSeriesSigma2->GetOutput();
	ImageType3D::Pointer ScaleResponses4 = joinSeriesSigma4->GetOutput();

	// MinProjection over scales
	ImageType::RegionType regionscale;
	ImageType::RegionType::SizeType sizescale;
	ImageType::RegionType::IndexType indexscale;
	ImageType3D::RegionType requestedRegionScale = ScaleResponses2->GetLargestPossibleRegion();

	indexscale[0] = requestedRegionScale.GetIndex()[0];
	indexscale[1] = requestedRegionScale.GetIndex()[1];
	sizescale[0] = requestedRegionScale.GetSize()[0];
	sizescale[1] = requestedRegionScale.GetSize()[1];
	regionscale.SetSize(sizescale);
	regionscale.SetIndex(indexscale);

	ImageType::Pointer outputScaleImage2 = ImageType::New();
	outputScaleImage2->SetRegions(regionscale);
	outputScaleImage2->Allocate();
	MaxProjection(ScaleResponses2, outputScaleImage2);

	ImageType::Pointer outputScaleImage4 = ImageType::New();
	outputScaleImage4->SetRegions(regionscale);
	outputScaleImage4->Allocate();
	MaxProjection(ScaleResponses4, outputScaleImage4);

	WriterType3D::Pointer writer2 = WriterType3D::New();
	writer2->SetFileName(argv[2]);
	writer2->SetInput(ScaleResponses2);

	WriterType3D::Pointer writer4 = WriterType3D::New();
	writer4->SetFileName(argv[3]);
	writer4->SetInput(ScaleResponses4);

	WriterType::Pointer writerdef2 = WriterType::New();
	writerdef2->SetFileName(argv[4]);
	writerdef2->SetInput(outputScaleImage2);

	WriterType::Pointer writerdef4 = WriterType::New();
	writerdef4->SetFileName(argv[5]);
	writerdef4->SetInput(outputScaleImage4);
	
	try
	{
		writer2->Update();
		writer4->Update();
		writerdef2->Update();
		writerdef4->Update();
	}
	catch (itk::ExceptionObject & exp)
	{
		std::cerr << "Exception caught !" << std::endl;
		std::cerr << exp << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
void MaxProjection(ImageType3D::Pointer inputImage, ImageType::Pointer outputImage)
{
	// Iterators
	SliceConstIteratorType  inputItz(inputImage, inputImage->GetLargestPossibleRegion());
	LinearIteratorType outputItz(outputImage, outputImage->GetLargestPossibleRegion());
	inputItz.SetFirstDirection(1);
	inputItz.SetSecondDirection(0);
	outputItz.SetDirection(1);
	outputItz.GoToBegin();
	while (!outputItz.IsAtEnd())
	{
		while (!outputItz.IsAtEndOfLine())
		{
			outputItz.Set(itk::NumericTraits<double>::NonpositiveMin());
			++outputItz;
		}
		outputItz.NextLine();
	}
	inputItz.GoToBegin();
	outputItz.GoToBegin();
	while (!inputItz.IsAtEnd())
	{
		while (!inputItz.IsAtEndOfSlice())
		{
			while (!inputItz.IsAtEndOfLine())
			{
				outputItz.Set(vnl_math_max(outputItz.Get(), inputItz.Get()));
				++inputItz;
				++outputItz;
			}
			outputItz.NextLine();
			inputItz.NextLine();
		}
		outputItz.GoToBegin();
		inputItz.NextSlice();
	}
}
void Sumer(ImageType3D::Pointer inputImage, ImageType3D::Pointer outputImage)
{
	// Iterators (Const allows only to get(), not set())
	SliceConstIteratorType  inputIts(inputImage, inputImage->GetLargestPossibleRegion());
	SliceIteratorType outputIts(outputImage, outputImage->GetLargestPossibleRegion());
	inputIts.SetFirstDirection(1);
	inputIts.SetSecondDirection(0);
	outputIts.SetFirstDirection(1);
	outputIts.SetSecondDirection(0);
	
	inputIts.GoToBegin();
	outputIts.GoToBegin();
	while (!inputIts.IsAtEnd())
	{
		while (!inputIts.IsAtEndOfSlice())
		{
			while (!inputIts.IsAtEndOfLine())
			{
				outputIts.Set(outputIts.Get() + inputIts.Get());
				++inputIts;
				++outputIts;
			}
			outputIts.NextLine();
			inputIts.NextLine();
		}
		outputIts.NextSlice();
		inputIts.NextSlice();
	}
}

