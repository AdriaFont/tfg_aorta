#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <itkImage.h>
#include <itkCurvatureFlowImageFilter.h>
#include <itkOpenCVImageBridge.h>
#include "itkImageFileReader.h"
#include <itkImageSliceIteratorWithIndex.h>
#include "itkExtractImageFilter.h"
#include "itkImageFileWriter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include <string.h>

typedef float                           InputPixelType;
typedef unsigned char                   OutputPixelType;
const unsigned int Dimension = 3;
typedef itk::Image< InputPixelType, Dimension >  InputImageType;
typedef itk::Image< InputPixelType, 2 >  SliceImageType;
typedef itk::Image< OutputPixelType, 2 >  SegImageType;
typedef itk::OpenCVImageBridge                   BridgeType;

typedef itk::ImageSliceConstIteratorWithIndex< InputImageType > SliceConstIteratorType;
typedef itk::ExtractImageFilter< InputImageType, SliceImageType > ExtractFilterType;
typedef itk::ImageFileReader<InputImageType> ReaderType;
typedef itk::ImageFileReader<SliceImageType> LabelReaderType;
typedef itk::ExtractImageFilter< InputImageType, SliceImageType > ExtractFilterType;
typedef itk::ImageFileWriter< SegImageType > WriterType;
typedef itk::RescaleIntensityImageFilter< SliceImageType, SliceImageType > RescaleFilterType;
typedef itk::StatisticsImageFilter<SliceImageType> StatisticsImageFilterType;

static void GetFeatureMat(InputImageType::Pointer inputImage, cv::Mat outputImage);

std::string type2str(int type) {
	std::string r;

	uchar depth = type & CV_MAT_DEPTH_MASK;
	uchar chans = 1 + (type >> CV_CN_SHIFT);

	switch (depth) {
	case CV_8U:  r = "8U"; break;
	case CV_8S:  r = "8S"; break;
	case CV_16U: r = "16U"; break;
	case CV_16S: r = "16S"; break;
	case CV_32S: r = "32S"; break;
	case CV_32F: r = "32F"; break;
	case CV_64F: r = "64F"; break;
	default:     r = "User"; break;
	}

	r += "C";
	r += (chans + '0');

	return r;
}
int  main(int argc, char **argv)
{


	if (argc < 2)
	{
		std::cout << "Usage: " << argv[0] << "< n (if 0 svm is loaded)> <inputFeatureStack1> <inputLabelImage1> ...<inputFeatureStackn> <inputLabelImagen>" << std::endl;
		std::cout << " <imageToPredict> <outputImage> < Save SVM (1 = true/ 0 = false)> <C> <Svm Name (*.xls)> <SvmMin (*.xls)> <SvmMax (*.xls)>" << std::endl;
		return -1;
	}

	const int TrainN = atoi(argv[1]);
	//TODO: Automatic resize
	cv::Mat NormMean(80, 1, CV_32FC1);
	cv::Mat NormStd(80, 1, CV_32FC1);
	cv::Mat NormMax(80, 1, CV_32FC1);
	cv::Mat NormMin(80, 1, CV_32FC1);
	bool svmLoad = !(bool)atoi(argv[1]);
	bool svmSave = (bool)atoi(argv[TrainN * 2 + 4]);
	int TotalPos = 0;
	if ((svmSave) && (svmLoad))
	{
		std::cout << "Either load or save the svm " << std::endl;
		return -1;
	}
	
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::LINEAR;
	params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 100000, 1e-5); // ITER or EPS
	params.C = atof(argv[TrainN * 2 + 5]);
	//params.gamma = 0.1;
	

	CvSVM SVM;
	
	if (svmLoad){
		const char * svmFileName = argv[TrainN * 2 + 6];
		const char * svmMinName = argv[TrainN * 2 + 7];
		const char * svmMaxName = argv[TrainN * 2 + 8];
		SVM.load(svmFileName);
		cv::FileStorage storage1(svmMinName, cv::FileStorage::READ);
		storage1["NormMin"] >> NormMin;
		cv::FileStorage storage2(svmMaxName, cv::FileStorage::READ);
		storage1["NormMax"] >> NormMax;
	}
	else{
		std::vector<cv::Mat> Featimages;
		std::vector<cv::Mat> Labimages;
		// TRAINING DATA:
		for (int iTN = 3; iTN < 2 + TrainN * 2; iTN = iTN + 2){


			ReaderType::Pointer reader = ReaderType::New();
			reader->SetFileName(argv[iTN - 1]);
			
			try
			{
				reader->Update();
			}
			catch (itk::ExceptionObject & excp)
			{
				std::cerr << excp << std::endl;
				return EXIT_FAILURE;
			}

			InputImageType::Pointer itkTrain = reader->GetOutput(); // itk training input Image

			// Input Sizes
			InputImageType::RegionType Inregion = reader->GetOutput()->GetLargestPossibleRegion();
			InputImageType::SizeType Insize = Inregion.GetSize();
			const int NumFeatures = Insize[2];
			const int NumPixelsX = Insize[0];
			const int NumPixelsY = Insize[1];
			const int trainingRows = NumPixelsX*NumPixelsY;
		

			cv::Mat trainingMatF(NumPixelsX*NumPixelsY, NumFeatures, CV_32F); // openCV training input Image
			GetFeatureMat(itkTrain, trainingMatF);
			Featimages.push_back(trainingMatF);

			// LABEL DATA:
			LabelReaderType::Pointer labelReader = LabelReaderType::New();
			labelReader->SetFileName(argv[iTN]);
			try
			{
				labelReader->Update();
			}
			catch (itk::ExceptionObject & excp)
			{
				std::cerr << excp << std::endl;
				return EXIT_FAILURE;
			}

			SliceImageType::Pointer itklabelMat = labelReader->GetOutput();
			cv::Mat labelMat = BridgeType::ITKImageToCVMat< SliceImageType >(itklabelMat);
			cv::Mat lab(NumPixelsX*NumPixelsY, 1, CV_32FC1);
			int countTotal = NumPixelsX*NumPixelsY;
			std::cout << "Heres ";
			int counterNeg = 0;
			int counterPos = 0;
			int iin = 0;
			for (int ik = 0; ik < labelMat.cols; ik++) {
				for (int jk = 0; jk < labelMat.rows; jk++) {
					float dope = labelMat.at<InputPixelType>(jk, ik);
					if (dope > 0){
						lab.at<float>(iin) = 1.0; // Not compulsory, done to keep the convention;
						counterPos = counterPos + 1;
					}
					else{
						lab.at<float>(iin) = -1.0;
						counterNeg = counterNeg + 1;
					}
					iin++;
				}
			}
			printf(" \n Negative: %i  Positive: %i , watch out unbalanced classes", counterNeg, counterPos);
			Labimages.push_back(lab);
			TotalPos = TotalPos + counterPos;
		}
		cv::Mat FeatConcat;
		cv::Mat LabConcat;
		cv::vconcat(Featimages, FeatConcat);
		cv::vconcat(Labimages, LabConcat);


		//--------------------------------------------------------------------------------------------------

		//Balance classes
		float percentage = (float)TotalPos / (float)(LabConcat.rows-TotalPos);
		printf(" \n Percentage Pos %f \n", percentage);

		cv::RNG rng(1);
		cv::Mat FeatBal;
		cv::Mat LabBal;
		for (int jb = 0; jb < LabConcat.rows; jb++) {
			cv::Mat inst = FeatConcat.row(jb);
			float labt = LabConcat.at<float>(jb);
			if (labt == 1){
				FeatBal.push_back(inst);
				LabBal.push_back(labt);
			}
			else{
				float b = rng.uniform(0.f, 1.f);
				if (b <= percentage){
					FeatBal.push_back(inst);
					LabBal.push_back(labt);

				}

			}

		}

		

		//Normalize with balance
		for (int is = 0; is < FeatBal.cols; is++) {
			cv::Scalar tmpmean;
			cv::Scalar tmpstd;
			meanStdDev(FeatBal.col(is), tmpmean, tmpstd);
			float myMATMean = tmpmean.val[0];
			float myMATStd = tmpstd.val[0];
			double myMATMin, myMATMax;
			
			NormMean.at<float>(is) = myMATMean;
			NormStd.at<float>(is) = myMATStd;
			
			// -------- Feature Scaling (need normalization in GetFeaturesMat()))
			cv::subtract(FeatBal.col(is), myMATMean, FeatBal.col(is));
			cv::multiply(FeatBal.col(is), 1.0 / (myMATStd), FeatBal.col(is));
			

			
			
			// --------- Feature Normalization (don't need normalization in GetFeaturesMat()?)
			cv::minMaxLoc(FeatBal.col(is), &myMATMin, &myMATMax);
			std::cout << "Min is: " << myMATMin << "Max is" << myMATMax << "\n";
			NormMin.at<float>(is) = myMATMin;
			NormMax.at<float>(is) = myMATMax;
			float myMATRange = myMATMax - myMATMin;
			cv::subtract(FeatBal.col(is), myMATMin, FeatBal.col(is));
			cv::multiply(FeatBal.col(is), 1.0 / (myMATRange), FeatBal.col(is));
			double minph, maxph;
			cv::minMaxLoc(FeatBal.col(is), &minph, &maxph);
			std::cout << "Min Norm is: " << minph << "Max Norm is" << maxph << "\n";
		}


		std::string tyx = type2str(FeatBal.type());
		printf("Balanced TRAINING Matrix is: %s %dx%d \n", tyx.c_str(), FeatBal.rows, FeatBal.cols);
		std::string ttw = type2str(LabBal.type());
		printf("Balanced LABEL Matrix is: %s %dx%d \n", ttw.c_str(), LabBal.rows, LabBal.cols);
		SVM.train(FeatBal, LabBal, cv::Mat(), cv::Mat(), params);

		//-----------------------------------------------------------------------------------------

		////Normalize
		//for (int is = 0; is < FeatConcat.cols; is++) {
		//	cv::Scalar tmpmean;
		//	cv::Scalar tmpstd;
		//    meanStdDev(FeatConcat.col(is),tmpmean,tmpstd);
		//	float myMATMean = tmpmean.val[0];
		//	float myMATStd = tmpstd.val[0];
		//	double myMATMin, myMATMax;
		//	cv::minMaxLoc(FeatConcat.col(is), &myMATMin, &myMATMax);
		//	std::cout << "Min is: " << myMATMin << "Max is" << myMATMax << "\n";
		//	NormMean.at<float>(is) = myMATMean;
		//	NormStd.at<float>(is) = myMATStd;
		//	NormMin.at<float>(is) = myMATMin;
		//	NormMax.at<float>(is) = myMATMax;
		//	// -------- Feature Scaling (need normalization in GetFeaturesMat()))
		//	/*cv::subtract(FeatBal.col(is), myMATMean, FeatBal.col(is));
		//	cv::multiply(FeatBal.col(is), 1.0 / (myMATStd), FeatBal.col(is));*/
		//	//float myMATRange = (myMATMax - myMATMean) / myMATStd - (myMATMin - myMATMean) / myMATStd;

		//	// --------- Feature Normalization (don't need normalization in GetFeaturesMat()?)
		//	float myMATRange = myMATMax - myMATMin;
		//	cv::subtract(FeatConcat.col(is), myMATMin, FeatConcat.col(is));
		//	cv::multiply(FeatConcat.col(is), 1.0 / (myMATRange), FeatConcat.col(is));
		//	double minph, maxph;
		//}
		//double minF, maxF;
		//cv::minMaxLoc(FeatConcat, &minF, &maxF);
		//std::cout << "Min is: "<< minF << "Max is" << maxF;
		//SVM.train(FeatConcat, LabConcat, cv::Mat(), cv::Mat(), params);

		/*std::string tyx = type2str(FeatConcat.type());
		printf("Balanced Matrix is: %s %dx%d \n", tyx.c_str(), FeatConcat.rows, FeatConcat.cols);
		std::string ttw = type2str(LabConcat.type());
		printf("Balanced Training Matrix is: %s %dx%d \n", ttw.c_str(), LabConcat.rows, LabConcat.cols);*/

		//-----------------------------------------------------------------------------------------------
		if (svmSave){
			const char * svmFileName = argv[TrainN * 2 + 6];
			SVM.save(svmFileName);
			//const char * svmMinName = argv[TrainN * 2 + 7];
			//const char * svmMaxName = argv[TrainN * 2 + 8];
			
			/*cv::FileStorage storage1(svmMinName, cv::FileStorage::WRITE);
			storage1 << "NormMin" <<NormMax;
			cv::FileStorage storage2("NormMax.yml", cv::FileStorage::WRITE);
			storage2 << "NormMax" << NormMin;
			std::cout << " \n SavedEverything ";*/

		}

	}
    

	// PREDICTION
	// Read stack to be predicted:
	ReaderType::Pointer predictionReader = ReaderType::New();
	predictionReader->SetFileName(argv[TrainN*2 +2]);
	predictionReader->Update();
	InputImageType::Pointer itkpredTrain = predictionReader->GetOutput(); // prediction input Image ITK

	// Input Sizes
	InputImageType::RegionType Predregion = itkpredTrain->GetLargestPossibleRegion();
	InputImageType::SizeType Predsize = Predregion.GetSize();
	const int NumFeaturesPred = Predsize[2]; // TODO: Write message error in case NumFeaturesPred =! NumFeatures and SVM is not loaded
	const int NumPixelsXPred = Predsize[0];
	const int NumPixelsYPred = Predsize[1];
	const int predictionRowsPred = NumPixelsXPred*NumPixelsYPred;
	std::cout << "NumFeatures " << NumFeaturesPred;
	std::cout << " NumPixelsYpred " << NumPixelsXPred;
	std::cout << " NumpixelsXpred " << NumPixelsYPred;

	cv::Mat predictionMat(predictionRowsPred, NumFeaturesPred, CV_32F);// prediction input Image OpenCV
	GetFeatureMat(itkpredTrain, predictionMat);
	
	float* mp = &NormMean.at<float>(0);
	
	for (int ip = 0; ip < predictionMat.cols; ip++) {
		
		// -------- Feature Scaling (need normalization in GetFeaturesMat()))
		
		cv::subtract(predictionMat.col(ip), NormMean.at<float>(ip), predictionMat.col(ip));
		cv::multiply(predictionMat.col(ip), 1.0 / NormStd.at<float>(ip), predictionMat.col(ip));

		// --------- Feature Normalization (don't need normalization in GetFeaturesMat()?)
		float RangeL = NormMax.at<float>(ip) - NormMin.at<float>(ip);
		cv::subtract(predictionMat.col(ip), NormMin.at<float>(ip), predictionMat.col(ip));
		cv::multiply(predictionMat.col(ip), 1.0 / RangeL, predictionMat.col(ip));

	}
	cv::Mat PredMat(NumPixelsYPred, NumPixelsXPred, CV_32F);
	cv::Mat PredMat2(NumPixelsYPred, NumPixelsXPred, CV_8UC1);

	int pc = 0;
	for (int i = 0; i < NumPixelsXPred; i++) {
		for (int j = 0; j <NumPixelsYPred; j++) {
			cv::Mat sample = predictionMat.row(pc);
			//std::cout << "Sample: " << SVM.predict(sample) << "\n";
			PredMat.at<float>(j, i) = SVM.predict(sample);
			if (SVM.predict(sample) > 0){
				PredMat2.at<unsigned char>(j, i) = 255;
			}
			else{
				PredMat2.at<unsigned char>(j, i) = 0;
			}
			pc++;
		}
	}

	//cv::imshow("SVM Output", PredMat2); // show it to the user
	//cv::waitKey(0);
	//cv::imwrite(argv[TrainN * 2 + 3], PredMat2);
	SegImageType::Pointer itkOutput = BridgeType::CVMatToITKImage< SegImageType >(PredMat2);
	std::cout << "Here " ;

	WriterType::Pointer writer = WriterType::New();
	writer->SetFileName(argv[TrainN * 2 + 3]);
	std::cout << "Here " ;

	writer->SetInput(itkOutput);
	try
	{
		writer->Update();
	}
	catch (itk::ExceptionObject & excp)
	{
		std::cerr << excp << std::endl;
		return EXIT_FAILURE;
	}
	

	std::string tpx = type2str(PredMat.type());
	printf("PRED Matrix is: %s %dx%d \n", tpx.c_str(), PredMat.rows, PredMat.cols);

	return EXIT_SUCCESS;
}
static void GetFeatureMat(InputImageType::Pointer inputImage, cv::Mat outputImage){
	 //Iterate over every Feature image to convert it to a feature column
	SliceConstIteratorType  ftIterator(inputImage, inputImage->GetLargestPossibleRegion());
	ftIterator.SetFirstDirection(1);
	ftIterator.SetSecondDirection(0);

	int FeatureCounter = 0;
	for (ftIterator.GoToBegin(); !ftIterator.IsAtEnd(); ftIterator.NextSlice())
	{
		//Setup region of the slice to extract
		InputImageType::IndexType sliceIndex = ftIterator.GetIndex();
		ExtractFilterType::InputImageRegionType::SizeType sliceSize = ftIterator.GetRegion().GetSize();
		sliceSize[2] = 0;
		ExtractFilterType::InputImageRegionType sliceRegion = ftIterator.GetRegion();
		sliceRegion.SetSize(sliceSize);
		sliceRegion.SetIndex(sliceIndex);

		//Pull out slice
		ExtractFilterType::Pointer inExtractor = ExtractFilterType::New();
		inExtractor->SetInput(inputImage);
		inExtractor->SetExtractionRegion(sliceRegion);
		inExtractor->SetDirectionCollapseToIdentity();
		inExtractor->Update();

		// Single Feature Scaling (how necessary is that? Disc P1/2)
		/*RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
		rescaleFilter->SetInput(inExtractor->GetOutput());
		rescaleFilter->SetOutputMinimum(0);
		rescaleFilter->SetOutputMaximum(1);
		rescaleFilter->Update();
		SliceImageType::Pointer itkSlice = rescaleFilter->GetOutput();*/
		SliceImageType::Pointer itkSlice = inExtractor->GetOutput();
		cv::Mat cvSlice = BridgeType::ITKImageToCVMat< SliceImageType >(itkSlice);
		//cv::imshow("SVM Simple Example", cvSlice); // show it to the user
		//cv::waitKey(0);

		/*double min, max;
		cv::minMaxLoc(cvSlice, &min, &max);
		std::cout << " openCV Min: " << min << std::endl;
		std::cout << " openCV Max: " << max << std::endl;*/

		int ii = 0; // Current column in training_mat
		for (int i = 0; i < cvSlice.cols; i++) {
			for (int j = 0; j < cvSlice.rows; j++) {
				outputImage.at<float>(ii++, FeatureCounter) = cvSlice.at<InputPixelType>(j, i);
				//printf("val %d", cvSlice.at<InputPixelType>(j, i));

			}

		}
		FeatureCounter = FeatureCounter + 1;
		
	}

}
