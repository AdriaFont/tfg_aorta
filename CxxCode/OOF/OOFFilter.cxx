
// This script uses the classes published in:
//	Benmansour, Fethallah, Engin T�retken, and Pascal Fua.
//	Tubular geodesics using oriented flux: an ITK implementation. No. EPFL-ARTICLE-183637. 2013.


//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http ://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkMultiScaleTubularityMeasureImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkShiftScaleImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkExpImageFilter.h"
//#include "itkOrientedFluxMatrixImageFilter.h"
#include "itkOrientedFluxCrossSectionTraceMeasure.h"


int main(int argc, char *argv[])
{
  if( argc < 7 )
    {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile outputimagefile scaleoutputimagefile sigmamin sigmamax sigmascale simgafix" << std::endl;
    return EXIT_FAILURE;
    }
  const unsigned int Dimension = 2;
  typedef float	  InputPixelType;
  
  typedef itk::Image< InputPixelType, 2 >         InputImageType;
  typedef itk::ImageFileReader<InputImageType>    ReaderType;
  typedef float																    OutputPixelType;
  typedef itk::Image<OutputPixelType, Dimension>								OutputImageType;
  typedef itk::Image<OutputPixelType, Dimension + 1>							OutputScaleSpaceImageType;


  typedef itk::ImageFileWriter<OutputImageType>								FileWriterType;
  typedef itk::ImageFileWriter<OutputScaleSpaceImageType> 		            ScaleSpaceImageFileWriterType;

  typedef float	OFPixelScalarType;
  typedef itk::SymmetricSecondRankTensor<OFPixelScalarType, Dimension >	  OFPixelType;

  typedef itk::Image< OFPixelType, Dimension >	                          OFImageType;
  typedef itk::Image<float, Dimension>                                    OutputImageType;
  typedef itk::Image<OFPixelType, Dimension>                              OrientedFluxImageType;
  typedef itk::Image<float, Dimension>                                    ScaleImageType;
  typedef itk::OrientedFluxCrossSectionTraceMeasureFilter
	  <OFImageType, OutputImageType >								      OFCrossSectionTraceObjectnessFilterType;

  typedef itk::MultiScaleTubularityMeasureImageFilter
	  <InputImageType,
	  OFImageType,
	  ScaleImageType,
	  OFCrossSectionTraceObjectnessFilterType,
	  OutputImageType >						                              OFCrossSectionTraceMultiScaleFilterType;

  
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);
  
  InputImageType::Pointer inputImage = reader -> GetOutput();
  
  OFCrossSectionTraceMultiScaleFilterType::Pointer ofMultiScaleFilter =
	  OFCrossSectionTraceMultiScaleFilterType::New();

  ofMultiScaleFilter->SetInput(inputImage);
  ofMultiScaleFilter->SetBrightObject(false);
  ofMultiScaleFilter->SetSigmaMinimum( atof(argv[4]) ); 
  ofMultiScaleFilter->SetSigmaMaximum(atof(argv[5]));
  ofMultiScaleFilter->SetNumberOfSigmaSteps(atof(argv[6]));
  ofMultiScaleFilter->SetFixedSigmaForOrientedFluxImage(atof(argv[7]));
  ofMultiScaleFilter->SetGenerateScaleOutput( true );
  ofMultiScaleFilter->SetGenerateOrientedFluxOutput( false );
  ofMultiScaleFilter->SetGenerateNPlus1DOrientedFluxOutput( false );
  ofMultiScaleFilter->SetGenerateNPlus1DOrientedFluxMeasureOutput(true );
  
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName(argv[2]);
  writer->SetInput(ofMultiScaleFilter -> GetOutput());

  FileWriterType::Pointer writer2 = FileWriterType::New();
  writer2->SetFileName("scale.mhd");
  writer2->SetInput(ofMultiScaleFilter->GetScaleOutput());

  OutputScaleSpaceImageType::Pointer scaleSpaceTubularityScoreImage;
  scaleSpaceTubularityScoreImage = ofMultiScaleFilter->GetNPlus1DImageOutput();
  ScaleSpaceImageFileWriterType::Pointer scaleSpaceWriter = ScaleSpaceImageFileWriterType::New();
  scaleSpaceWriter->SetFileName(argv[3]);
  scaleSpaceWriter->SetInput(scaleSpaceTubularityScoreImage);
  try
  {
	  writer->Update();
	  writer2->Update();
	  scaleSpaceWriter->Update();
  }
  catch (itk::ExceptionObject &e)
  {
	  std::cerr << e << std::endl;
  }
  
 
 
  return EXIT_SUCCESS;
}