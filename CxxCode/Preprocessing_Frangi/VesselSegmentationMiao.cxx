
// Adri� Font Calvarons

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http ://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

#include "iostream"

#include "itkImage.h"
#include "vnl/vnl_math.h"
#include "itkImageSeriesReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"
#include "itkImageSliceConstIteratorWithIndex.h"
#include "itkImageLinearIteratorWithIndex.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkPNGImageIO.h"
#include "itkTIFFImageIO.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkInvertIntensityImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkLabelShapeKeepNObjectsImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkHessianToObjectnessMeasureImageFilter.h" // If error encountered, check:       
#include "itkMultiScaleHessianBasedMeasureImageFilter.h" // If error encountered, check:  
// http://public.kitware.com/pipermail/insight-developers/2013-August/023053.html
#include "itkOtsuThresholdImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkConnectedComponentImageFilter.h"
#include "itkMedianImageFilter.h"
#include <vector>
#include <numeric> 
#include <itkJoinSeriesImageFilter.h>
#include "QuickView.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkImageRegionConstIteratorWithIndex.h"
#include "itkOtsuMultipleThresholdsCalculator.h"
#include "itkScalarImageToHistogramGenerator.h"
#include "itkNumericTraits.h"
#include "itkImageLinearIteratorWithIndex.h"
#include "itkImageSliceIteratorWithIndex.h"
#include <itkStatisticsImageFilter.h>
#include <itkExtractImageFilter.h>

const unsigned int Dimension = 2;
typedef float                             PixelType;
typedef unsigned int                       PixelTypeRead;
typedef itk::Image< PixelTypeRead, 3 >  ImageType3D;
typedef itk::Image< PixelType, 3 >  ImageVolType;
typedef itk::Image< PixelTypeRead, Dimension > ImageType2D;
typedef itk::Image< PixelType, Dimension > ImageType;

typedef itk::ImageFileWriter< ImageType > WriterTypeThr;


typedef itk::ImageLinearIteratorWithIndex< ImageType2D >     LinearIteratorType;
typedef itk::ImageSliceConstIteratorWithIndex< ImageType3D > SliceConstIteratorType;

typedef itk::ImageLinearIteratorWithIndex< ImageType >     LinearScaleIteratorType;
typedef itk::ImageSliceIteratorWithIndex< ImageVolType > SliceIteratorType;

static void MinProjection(ImageType3D::Pointer inputImage, ImageType2D::Pointer outputImage);
static void LinearMappingThresholds(ImageType::Pointer inputImage, float& lowerThreshold, float& upperThreshold, bool brightness, const float thr);
int BlockSelection(int imageSize, int minBlock);
float ComputeVariance(std::vector<float> Vect, float mean, float sum);

int main(int argc, char * argv[])
{
	if (argc < 13)
	{
		std::cerr << "Usage: " << std::endl;
		std::cerr << argv[0] << " inputFileName (if series: %03d) InputType(series = 1/ single = 0)"<< std::endl
			<< "  firstSliceValue(if stack set parameter is ignored) lastSliceValue(if stack parameter is ignored) Inputstacktype (numeric png series = 1 / single stack = 0)" << std::endl
			<< " brightness varthreshold sigmaMinimum sigmaMaximum sigmasteps" << std::endl
			<< " intermediateFileName outFileName outScaleFileName " << std::endl;
		return EXIT_FAILURE;
	}
	typedef itk::ImageSeriesReader< ImageType3D >  VolReaderType;

	typedef itk::ImageFileReader< ImageType2D >  ReaderType;
	typedef itk::ImageFileReader< ImageType3D >  ReaderType3;

	typedef itk::NumericSeriesFileNames    NameGeneratorType;

	typedef itk::RescaleIntensityImageFilter< ImageType2D, ImageType2D > RescaleType;

	typedef itk::CastImageFilter< ImageType2D, ImageType > ImageUncharCastFilter;

	typedef itk::GradientAnisotropicDiffusionImageFilter< ImageType, ImageType > AnisFilterType;

	typedef itk::IntensityWindowingImageFilter <ImageType, ImageType> IntensityWindowingImageFilterType; //here

	typedef itk::ImageFileWriter< ImageType2D > WriterTypeInt;

	typedef itk::ImageFileWriter< ImageType > WriterTypefr;

	typedef itk::CastImageFilter< ImageType2D, ImageType > FilterType;
	
	typedef itk::MedianImageFilter<ImageType, ImageType > MedianFilterType;

	typedef itk::JoinSeriesImageFilter< ImageType, ImageVolType> JoinSeriesFilterType;


	typedef itk::SymmetricSecondRankTensor< double, Dimension > HessianPixelType;
	typedef itk::Image< HessianPixelType, Dimension >           HessianImageType;
	typedef itk::HessianToObjectnessMeasureImageFilter< HessianImageType, ImageType >
		ObjectnessFilterType;
	typedef itk::MultiScaleHessianBasedMeasureImageFilter< ImageType, HessianImageType, ImageType >
		MultiScaleEnhancementFilterType;
    
    typedef std::vector<ImageType::Pointer>				 VectorImagePointerType;

	typedef itk::RescaleIntensityImageFilter< ImageType, ImageType >
		RescaleFilterType;

	typedef itk::ImageFileWriter< ImageType > WriterTypefr;
	typedef itk::ImageFileWriter< ImageVolType > WriterTypeSc;

	const char * outputFileNameScaleResponses = argv[14];
	const char * outputFileName = argv[12];
	const char * outputFileNamesc = argv[13];
	const char * intermFileName = argv[11];
	const char * InputFileName = argv[1];
	
	// const char * seriesFileName = "./BrainDSA/ImageBraindivided_%03d.png";
	bool imageSeriesInput = (bool)atoi(argv[2]);
	bool numericpng = (bool)atoi(argv[5]);


	ImageType2D::Pointer outputImage = ImageType2D::New();
	if (imageSeriesInput){
		ImageType3D::Pointer inputImage;
		if (numericpng){

			VolReaderType::Pointer reader3 = VolReaderType::New();
			const unsigned int first = atoi(argv[3]);
			const unsigned int last = atoi(argv[4]);


			//Read the pattern files and build 3D volume

			NameGeneratorType::Pointer nameGenerator = NameGeneratorType::New();

			nameGenerator->SetSeriesFormat(InputFileName);
			//nameGenerator->SetSeriesFormat("./BrainDSA/ImageBrain_%03d.png");
			nameGenerator->SetStartIndex(first);
			nameGenerator->SetEndIndex(last);
			nameGenerator->SetIncrementIndex(1);

			reader3->SetImageIO(itk::TIFFImageIO::New()); // If another input image type, replace this

			reader3->SetFileNames(nameGenerator->GetFileNames());
			

			try
			{
				reader3->Update();
				
			}
			catch (itk::ExceptionObject & err)
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << err << std::endl;
				return EXIT_FAILURE;
			}
			inputImage = reader3->GetOutput();
		}
		else{
			std::cout << "hereee?" << std::endl;
			ReaderType3::Pointer reader3 = ReaderType3::New();
			reader3->SetFileName(argv[1]);
			try
			{
				reader3->Update();
				
			}
			catch (itk::ExceptionObject & err)
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << err << std::endl;
				return EXIT_FAILURE;
			}
			inputImage = reader3->GetOutput();
		}
		// Maximum Opacity Image (inputImage): Based on itk Slice Iterator example

		ImageType2D::RegionType region;
		ImageType2D::RegionType::SizeType size;
		ImageType2D::RegionType::IndexType index;
		ImageType3D::RegionType requestedRegion = inputImage->GetRequestedRegion();
		std::cout << " size: " << requestedRegion.GetSize()[0];
		std::cout << " size: " << requestedRegion.GetSize()[1];
		index[0] = requestedRegion.GetIndex()[0];
		index[1] = requestedRegion.GetIndex()[1];
		size[0] = requestedRegion.GetSize()[0];
		size[1] = requestedRegion.GetSize()[1];
		region.SetSize(size);
		region.SetIndex(index);
		outputImage->SetRegions(region);
		outputImage->Allocate();


		MinProjection(inputImage, outputImage);
		
	}
	else{
		ReaderType::Pointer reader = ReaderType::New();
		reader->SetFileName(argv[1]);
		try
		{
			reader->Update();
			outputImage = reader->GetOutput();
		}
		catch (itk::ExceptionObject & err)
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}
	}

	FilterType::Pointer filterCast = FilterType::New();
	filterCast->SetInput(outputImage);
	ImageType::Pointer imageCasted = filterCast->GetOutput();
	
	//Linear Mapping (0-1)
	RescaleFilterType::Pointer rescaleFilterPre = RescaleFilterType::New();
	rescaleFilterPre->SetInput(imageCasted);
	rescaleFilterPre->SetOutputMinimum(0);
	rescaleFilterPre->SetOutputMaximum(1);

	// Gradient anisotropic filter 

	const int numberOfIterations = 15.0;
	const PixelType timeStep = 0.125;
	const PixelType conductance = 1.0;
	AnisFilterType::Pointer filterAnis = AnisFilterType::New();
	filterAnis->SetInput(rescaleFilterPre->GetOutput());
	filterAnis->SetNumberOfIterations(numberOfIterations);
	filterAnis->SetTimeStep(timeStep);
	filterAnis->SetConductanceParameter(conductance);
	filterAnis->Update(); // Important here otherwise ComputeBlock inside LinearMapping fails as image has size 0 0

	MedianFilterType::Pointer medianFilter = MedianFilterType::New();
	medianFilter->SetRadius(2);
	medianFilter->SetInput(filterAnis->GetOutput());
	medianFilter->Update();
	ImageType::Pointer imageAnis = medianFilter->GetOutput();

	//QuickView viewer1;
	//viewer1.AddImage(filterAnis->GetOutput(), true, " 1 maximumOpacity");
	//viewer1.Visualize();
	bool brightness = (bool)atoi(argv[6]);
	const float varthreshold = atof(argv[7]);
	float LowerThreshold,UpperThreshold;
	LinearMappingThresholds(imageAnis, LowerThreshold, UpperThreshold, brightness, varthreshold);
	std::cout << "lower: " << LowerThreshold << std::endl;
	std::cout << "upper: " << UpperThreshold << std::endl;
	IntensityWindowingImageFilterType::Pointer filterMapping = IntensityWindowingImageFilterType::New();
	filterMapping->SetInput(imageAnis);
	filterMapping->SetWindowMinimum(LowerThreshold);
	filterMapping->SetWindowMaximum(UpperThreshold);
	filterMapping->SetOutputMinimum(0);
	filterMapping->SetOutputMaximum(1);
	ImageType::Pointer imageMap= filterMapping->GetOutput();

	// Intermedate write (Get mapped maximum opacity useful for other techniques)
	
	WriterTypefr::Pointer writerInterm = WriterTypefr::New();
	writerInterm->SetFileName(intermFileName);
	writerInterm->SetInput(imageMap);

	try
	{
		writerInterm->Update();
	}
	catch (itk::ExceptionObject & error)
	{
		std::cerr << "Error: " << error << std::endl;
		return EXIT_FAILURE;
	}
	
	printf(" intermediate written");
	//ImageUncharCastFilter::Pointer filterCastback = ImageUncharCastFilter::New();
	//filterCastback->SetInput(imageMap);
	//ImageType::Pointer imageCasted2 = filterCastback->GetOutput();
	
	

	// Frangi
	 
	double sigmaMinimum = atoi(argv[8]);
	double sigmaMaximum = atoi(argv[9]);
	unsigned int numberOfSigmaSteps = atoi(argv[10]);
	printf("here");


	ObjectnessFilterType::Pointer objectnessFilter = ObjectnessFilterType::New();
	objectnessFilter->SetBrightObject(false);
	objectnessFilter->SetScaleObjectnessMeasure(false);
	//objectnessFilter->SetAlpha(0.5);
	objectnessFilter->SetBeta(1.0);
	objectnessFilter->SetGamma(0.5);

	
	MultiScaleEnhancementFilterType::Pointer multiScaleEnhancementFilter =
		MultiScaleEnhancementFilterType::New();
	multiScaleEnhancementFilter->SetInput(imageMap);
	multiScaleEnhancementFilter->SetHessianToMeasureFilter(objectnessFilter);
	multiScaleEnhancementFilter->SetSigmaStepMethodToEquispaced();
	multiScaleEnhancementFilter->SetSigmaMinimum(sigmaMinimum);
	multiScaleEnhancementFilter->SetSigmaMaximum(sigmaMaximum);
	multiScaleEnhancementFilter->SetNumberOfSigmaSteps(numberOfSigmaSteps);
	multiScaleEnhancementFilter->SetNonNegativeHessianBasedMeasure(true);
	multiScaleEnhancementFilter->GenerateScalesOutputOn();
	multiScaleEnhancementFilter->SetGenerateScalesOutput(true);
	try
	{
		multiScaleEnhancementFilter->Update();
	}
	catch (itk::ExceptionObject & error)
	{
		std::cerr << "Error: " << error << std::endl;
		return EXIT_FAILURE;
	}
	printf("heret\n");
    
    
    VectorImagePointerType frangiScalesImages;
    multiScaleEnhancementFilter->GetScaleResponsesImageVector(frangiScalesImages);
    
    std::cout << "number of scales: " << frangiScalesImages.size() << std::endl;

	JoinSeriesFilterType::Pointer joinSeries = JoinSeriesFilterType::New();
	joinSeries->SetOrigin(0);
	joinSeries->SetSpacing(1);

	for (std::vector<ImageType::Pointer>::iterator it = frangiScalesImages.begin(); it != frangiScalesImages.end(); ++it){
		ImageType::Pointer current = *it ;
		joinSeries->PushBackInput(current);
	}
	joinSeries->Update();
	ImageVolType::Pointer scaleResponses = joinSeries->GetOutput();

	WriterTypeSc::Pointer writerScales = WriterTypeSc::New();
	writerScales->SetFileName(outputFileNameScaleResponses);
	writerScales->SetInput(scaleResponses); //save Frangi Output
	//Rescale frangi output and Convert

	RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
	rescaleFilter->SetInput(multiScaleEnhancementFilter->GetOutput());
	rescaleFilter->SetOutputMinimum(0);
	rescaleFilter->SetOutputMaximum(1);

	RescaleFilterType::Pointer rescaleFiltersc = RescaleFilterType::New();
	rescaleFiltersc->SetInput(multiScaleEnhancementFilter->GetScalesOutput());
	rescaleFiltersc->SetOutputMinimum(0);
	rescaleFiltersc->SetOutputMaximum(1);
	
	WriterTypefr::Pointer writerfr = WriterTypefr::New();
	writerfr->SetFileName(outputFileName);
	writerfr->SetInput(rescaleFilter->GetOutput()); //save Frangi Output

	WriterTypefr::Pointer writersc = WriterTypefr::New();
	writersc->SetFileName(outputFileNamesc);
	writersc->SetInput(rescaleFiltersc->GetOutput()); //save Frangi Output Scale

	/*WriterTypefr::Pointer writers0 = WriterTypefr::New();
	writers0->SetFileName("test0.mhd");
	writers0->SetInput(frangiScalesImages[0]);

	WriterTypefr::Pointer writers1 = WriterTypefr::New();
	writers1->SetFileName("test1.mhd");
	writers1->SetInput(frangiScalesImages[1]);

	WriterTypefr::Pointer writers2 = WriterTypefr::New();
	writers2->SetFileName("test2.mhd");
	writers2->SetInput(frangiScalesImages[2]);

	WriterTypefr::Pointer writers3 = WriterTypefr::New();
	writers3->SetFileName("test3.mhd");
	writers3->SetInput(frangiScalesImages[3]);*/
	// Otsu
	//typedef unsigned char PixelTypeOutt;
	//typedef itk::Image< unsigned short, Dimension > OutputImageTypeShort;

	//typedef itk::OtsuThresholdImageFilter <OutputImageType, OutputImageType>
	//	OtsuFilterType;
	//OtsuFilterType::Pointer otsuFilter
	//	= OtsuFilterType::New();
	//otsuFilter->SetInput(rescaleFilter->GetOutput());

	////threshold (invert function)
	//typedef itk::BinaryThresholdImageFilter <OutputImageType, OutputImageType>
	//	BinaryThresholdImageFilterType;

	//BinaryThresholdImageFilterType::Pointer thresholdFilter
	//	= BinaryThresholdImageFilterType::New();
	//thresholdFilter->SetInput(otsuFilter->GetOutput());
	//thresholdFilter->SetUpperThreshold(itk::NumericTraits<PixelTypeOutt>::max()/2);
	//thresholdFilter->SetInsideValue(255);
	//thresholdFilter->SetOutsideValue(0);

	//// Connected Components
	//typedef itk::ConnectedComponentImageFilter <OutputImageType, OutputImageTypeShort >
	//	ConnectedComponentImageFilterType;

	//ConnectedComponentImageFilterType::Pointer connected = ConnectedComponentImageFilterType::New();
	//connected->SetInput(thresholdFilter->GetOutput());
	//connected->Update();

	//std::cout << "Number of objects: " << connected->GetObjectCount() << std::endl;

	//typedef itk::LabelShapeKeepNObjectsImageFilter< OutputImageTypeShort > LabelShapeKeepNObjectsImageFilterType;
	//LabelShapeKeepNObjectsImageFilterType::Pointer labelShapeKeepNObjectsImageFilter = LabelShapeKeepNObjectsImageFilterType::New();
	//labelShapeKeepNObjectsImageFilter->SetInput(connected->GetOutput());
	//labelShapeKeepNObjectsImageFilter->SetBackgroundValue(0);
	//labelShapeKeepNObjectsImageFilter->SetNumberOfObjects(1);
	//labelShapeKeepNObjectsImageFilter->SetAttribute(LabelShapeKeepNObjectsImageFilterType::LabelObjectType::NUMBER_OF_PIXELS);

	//typedef itk::RescaleIntensityImageFilter< OutputImageTypeShort, OutputImageType > RescaleFilterTypeCon;
	//RescaleFilterTypeCon::Pointer rescaleFilterCon = RescaleFilterTypeCon::New();
	//rescaleFilterCon->SetOutputMinimum(0);
	//rescaleFilterCon->SetOutputMaximum(itk::NumericTraits<PixelTypeOutt>::max());
	//rescaleFilterCon->SetInput(labelShapeKeepNObjectsImageFilter->GetOutput());

	/*QuickView viewer1;
	viewer1.AddImage(filterAnis->GetOutput(), true, " 1 maximumOpacity");
	QuickView viewer2;
	viewer2.AddImage(filterMapping->GetOutput(), true, " 2 linear mapping");
	QuickView viewer3;
	viewer3.AddImage(rescaleFilter->GetOutput(), true, " 3 Frangi");
	QuickView viewer4;
	viewer4.AddImage(thresholdFilter->GetOutput(), true, " 4 threshold Otsu");
	QuickView viewer5;
	viewer5.AddImage(rescaleFilterCon->GetOutput(), true, " 5 Connected");
	viewer1.Visualize();
	viewer2.Visualize();
	viewer3.Visualize();
	viewer4.Visualize();
	viewer5.Visualize();*/

	//Write
	/*typedef itk::ImageFileWriter< OutputImageType > WriterType;
	WriterType::Pointer writer = WriterType::New();
	writer->SetFileName(outputFileName);
	writer->SetInput(rescaleFilterCon->GetOutput());*/ //
	

	try
	{
		writerfr->Update();
		writersc->Update();
		writerScales->Update();
		/*writers0->Update();
		writers1->Update();
		writers2->Update();
		writers3->Update();*/
		
	}
	catch (itk::ExceptionObject & error)
	{
		std::cerr << "Error: " << error << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
	
}
void MinProjection(ImageType3D::Pointer inputImage, ImageType2D::Pointer outputImage)
{
	// Iterators
	SliceConstIteratorType  inputItz(inputImage, inputImage->GetLargestPossibleRegion());
	LinearIteratorType outputItz(outputImage, outputImage->GetLargestPossibleRegion());
	inputItz.SetFirstDirection(1);
	inputItz.SetSecondDirection(0);
	outputItz.SetDirection(1);
	outputItz.GoToBegin();
	while (!outputItz.IsAtEnd())
	{
		while (!outputItz.IsAtEndOfLine())
		{
			outputItz.Set(itk::NumericTraits<PixelTypeRead>::max());
			//outputItz.Set(0);
			++outputItz;
		}
		outputItz.NextLine();
	}
	inputItz.GoToBegin();
	outputItz.GoToBegin();
	while (!inputItz.IsAtEnd())
	{
		while (!inputItz.IsAtEndOfSlice())
		{
			while (!inputItz.IsAtEndOfLine())
			{
				outputItz.Set(vnl_math_min(outputItz.Get(), inputItz.Get()));
				//printf(" outputval = %f", inputItz.Get());
				++inputItz;
				++outputItz;
			}
			outputItz.NextLine();
			inputItz.NextLine();
		}
		outputItz.GoToBegin();
		inputItz.NextSlice();
	}
}
 void LinearMappingThresholds(ImageType::Pointer inputImage, float& lowerThreshold, float& upperThreshold, bool brightness, const float thr )
{
	typedef itk::ExtractImageFilter< ImageType, ImageType > FilterType;
	typedef itk::StatisticsImageFilter<ImageType> StatisticsImageFilterType;
	typedef itk::Statistics::ScalarImageToHistogramGenerator<
		ImageType > ScalarImageToHistogramGeneratorType;
	typedef ScalarImageToHistogramGeneratorType::HistogramType HistogramType;
	typedef itk::OtsuMultipleThresholdsCalculator< HistogramType >
		CalculatorType;
	typedef itk::RegionOfInterestImageFilter< ImageType, ImageType > RoiFilterType;


	ImageType::Pointer image = inputImage;
	ImageType::RegionType Inregion = image->GetLargestPossibleRegion();
	ImageType::SizeType Insize = Inregion.GetSize();
	const int NumPixelsX = Insize[0];
	const int NumPixelsY = Insize[1];
	std::cout << "Image largest region: " << image->GetLargestPossibleRegion() << std::endl;

	// Create empty matrix
	ImageType::Pointer imageThr = ImageType::New();
	imageThr->SetRegions(Inregion);
	imageThr->Allocate();
	imageThr->FillBuffer(itk::NumericTraits< PixelType >::Zero);
	//std::cout << "   Thr Image largest region: " << imageThr->GetLargestPossibleRegion() << std::endl;



	// Block Size and Block Step; 
	const int minimumBlockSizeX = (int)NumPixelsX/13;
	const int minimumBlockSizeY = (int)NumPixelsY/13;
	int blockSizeX = BlockSelection(NumPixelsX, minimumBlockSizeX);
	int blockSizeY = BlockSelection(NumPixelsY, minimumBlockSizeY);
	const int minimumBlockStepX = (int)blockSizeX/ 7;
	const int minimumBlockStepY = (int)blockSizeY/ 7;
	int blockStepX = BlockSelection(blockSizeX, minimumBlockStepX);
	int blockStepY = BlockSelection(blockSizeY, minimumBlockStepY);

	ImageType::SizeType sizeBlock;
	sizeBlock[0] = blockSizeX;
	sizeBlock[1] = blockSizeY;
	std::cout << "blockSizeX: " << blockSizeX << std::endl;
	std::cout << "blockSizeY: " << blockSizeY << std::endl;
	std::cout << "blockStepX: " << blockStepX << std::endl;
	std::cout << "blockStepY: " << blockStepY << std::endl;

	//const float thr = 0.09;

	for (int i = 0; i <= NumPixelsX - blockSizeX; i = i + blockStepX){
		for (int j = 0; j <= NumPixelsY - blockSizeY; j = j + blockStepY){

			//printf(" i = %i j = %i \n", i, j);

			// Set up roi
			ImageType::IndexType startBlock;
			startBlock[0] = i;
			startBlock[1] = j;
			ImageType::RegionType desiredRegion;
			desiredRegion.SetSize(sizeBlock);
			desiredRegion.SetIndex(startBlock);

			// Extract roi
			RoiFilterType::Pointer roiFilter = RoiFilterType::New();
			roiFilter->SetRegionOfInterest(desiredRegion);
			roiFilter->SetInput(image);
			roiFilter->Update();
			//std::cout << "Image region: " << roiFilter->GetOutput()->GetLargestPossibleRegion() << std::endl;
			// Compute roi's variance
			StatisticsImageFilterType::Pointer statisticsImageFilter
				= StatisticsImageFilterType::New();
			statisticsImageFilter->SetInput(roiFilter->GetOutput());
			statisticsImageFilter->Update();
			float varBlock = statisticsImageFilter->GetSigma();
			//printf(" varBlock = %f", varBlock);
			if (varBlock > thr){
				//std::cout << "in " << blockSizeX << std::endl;
				// Compute roi's Otsu threshold
				ScalarImageToHistogramGeneratorType::Pointer scalarImageToHistogramGenerator
					= ScalarImageToHistogramGeneratorType::New();
				CalculatorType::Pointer calculator = CalculatorType::New();
				scalarImageToHistogramGenerator->SetNumberOfBins(64);
				calculator->SetNumberOfThresholds(1);
				scalarImageToHistogramGenerator->SetInput(roiFilter->GetOutput());
				calculator->SetInputHistogram(
					scalarImageToHistogramGenerator->GetOutput());

				scalarImageToHistogramGenerator->Compute();
				try
				{
					calculator->Compute();
				}
				catch (itk::ExceptionObject & excp)
				{
					std::cerr << "Exception thrown " << excp << std::endl;
				}
				CalculatorType::OutputType thresholdVector = calculator->GetOutput();

				//std::cout << "threshold is: " << thresholdVector[0] << std::endl;


				// Fill imageThr
				itk::ImageRegionConstIteratorWithIndex<ImageType> blockIt(roiFilter->GetOutput(), roiFilter->GetOutput()->GetLargestPossibleRegion());
				itk::ImageRegionIteratorWithIndex<ImageType> thrIt(imageThr, desiredRegion);
				blockIt.Begin();
				thrIt.Begin();
				while (!blockIt.IsAtEnd()){
					PixelType currentVal = blockIt.Get();
					if (brightness) { //bright
						if (currentVal > thresholdVector[0]){
							thrIt.Set(1 + thrIt.Get());

						}
					}
					else {   //dark
						if (currentVal < thresholdVector[0]){
							thrIt.Set(1 + thrIt.Get());
						}

					}
					++thrIt;
					++blockIt;
				}
			}
		}
	}


	StatisticsImageFilterType::Pointer statisticsImageFilterMax
		= StatisticsImageFilterType::New();
	statisticsImageFilterMax->SetInput(imageThr);
	statisticsImageFilterMax->Update();
	float maxThr = statisticsImageFilterMax->GetMaximum();
	std::cout << "maxnumberofcounts" << maxThr<< std::endl;
	const float voteFactor = 4.0;
	const float thr2 = maxThr / voteFactor;

	WriterTypeThr::Pointer writerThr = WriterTypeThr::New();
	writerThr->SetFileName("Thr.mhd");
	writerThr->SetInput(imageThr);
	writerThr->Update();
	
	std::vector<float> foreground;
	std::vector<float> background;
	itk::ImageRegionConstIteratorWithIndex<ImageType> origIt(image, Inregion);
	itk::ImageRegionConstIteratorWithIndex<ImageType> threshdIt(imageThr, Inregion);
	origIt.Begin();
	threshdIt.Begin();
	while (!origIt.IsAtEnd()){
		PixelType currentthrVal = threshdIt.Get();
		PixelType currentoriVal = origIt.Get();
		if (currentthrVal < thr2){
			foreground.push_back(currentoriVal);
		}
		else{
			background.push_back(currentoriVal);
		}
		++origIt;
		++threshdIt;
	}


	//for (std::vector<float>::const_iterator i = background.begin(); i != background.end(); ++i)
	// std::cout << *i << ' ';
	float foreSum = std::accumulate(foreground.begin(), foreground.end(), 0.0); //Very important to specify a float 0.0
	float foreMean = foreSum / foreground.size();
	float backSum = std::accumulate(background.begin(), background.end(), 0.0);//Very important to specify a float 0.0
	float backMean = backSum / background.size();
	float foreVar = ComputeVariance(foreground, foreMean, foreSum);
	float backVar = ComputeVariance(background, backMean, backSum);
	std::cout << "backVar: " << backVar << std::endl;
	std::cout << "backMean: " << backMean << std::endl;
	lowerThreshold = backMean - backVar;
	upperThreshold = foreMean + foreVar;
}
int BlockSelection(int imageSize, int minBlock){
	int retval;
	

	for (int x = minBlock; x <= imageSize; x++)
	{
		if (imageSize % x == 0)
		{
			retval = x;
			break;
		}
	}
	return retval;
}
float ComputeVariance(std::vector<float> Vect, float mean, float sum){
	float temp = 0;
	float currentValue;
	for (std::vector<float>::iterator it = Vect.begin(); it != Vect.end(); ++it)
	{
		currentValue = *it;
		//printf(" currentValue = %f", currentValue);
		temp = temp + pow(currentValue - mean, 2);
		//printf(" temp = %f", temp);

	}
	return temp / sum;
}